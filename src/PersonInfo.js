import React from 'react';
import './PersonInfo.css';

import Form from 'react-bootstrap/Form';

function PersonInfo(props) {
  var person = props.person;
  
  var firstName, lastName, district, phone, office;
  if (!person) {
    firstName = 'First Name';
    lastName  = 'Last Name';
    district  = 'District';
    phone     = 'Phone';
    office    = 'Office';
  }
  else {
    // FIXME! Will this handle last names being multiple words like "Van Damme"?
    var res  = person.name.split(" ");
    firstName = res.shift();
    lastName  = res.join(" ");
    district  = person.district;
    phone     = person.phone;
    office    = person.office;
  }

  return (
    <div className="PersonInfo">
      <h3>Info</h3>
      <Form.Group>
        <Form.Control
          type="text"
          readOnly
          style={{marginBottom: ".5em"}}
          value={firstName}
        />
        <Form.Control
          type="text"
          readOnly
          style={{marginBottom: ".5em"}}
          value={lastName}
        />
        <Form.Control
          type="text"
          readOnly
          style={{marginBottom: ".5em"}}
          value={district}
        />
        <Form.Control
          type="text"
          readOnly
          style={{marginBottom: ".5em"}}
          value={phone}
        />
        <Form.Control
          type="text"
          readOnly
          value={office}
        />
      </Form.Group>
    </div>
  );
}

export default PersonInfo;
