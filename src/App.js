import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from 'react-bootstrap/Container';

import Search from './Search';

function App() {
  return (
    <div className="App">
      <Container>
        <h1>Who's My Representative</h1>
        <Search />
      </Container>
    </div>
  );
}

export default App;
